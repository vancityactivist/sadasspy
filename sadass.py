from twilio import TwilioRestException
from twilio.rest import TwilioRestClient
import time
import configparser
import os
import sys
import hashlib
import uuid
import logging
from getpass import getpass
from datetime import datetime

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

## Load the config
config = configparser.SafeConfigParser()
config.read("config/config.cfg")

## Setup logging
logfile = config.get('logging', 'filename')
logging.basicConfig(filename='+logfile+', level=logging.DEBUG)

def sendSMS ( message, name, number ):


	account_sid = config.get('twilio', 'accountSID')
	auth_token  = config.get('twilio', 'authToken')
	outgoingNum = config.get('twilio','outgoingNum')

	client = TwilioRestClient(account_sid, auth_token)

	try:
		message = client.messages.create(body=message,
	    	to="+1"+number,    # Replace with your phone number
	    	from_="+1"+outgoingNum) # Replace with your Twilio number
	except TwilioRestException as e:
		print(e)
	return;

def sendTestAlert ( message ):
	smsQueue = config.items("contacts")
	parsedMessage = "****"+config.get('settings','displayname')+"****  "+message
	for name, number in smsQueue:
		name = str.capitalize(name)
		print("Sending to " + name + " now at " + number)
		sendSMS(parsedMessage, name, number)
		time.sleep(1)
		print("Successfully sent the message: "+ parsedMessage +" to " + name)
		time.sleep(4)

def sensorStatusCheck ( sensor ):
	os.system('clear')
	print("Waiting for signal from sensor: "+sensor+". Please activate the trigger now.")
	print("(15 second timeout in effect)")
	sensorTest = GPIO.wait_for_edge(sensor, GPIO.RISING, timeout=15000)
	if sensorTest in None:
		print("Timeout occured. Please restart the test.")
	else:
		print("Sensor trigger detected on sensor: "+sensor+".")
		time.sleep(5)

def checkSensor():
	exit


def mainMenu():
	os.system('clear')
	print("Welcome to the SADASS Monitoring System")
	print("Designed by Porus Consulting Group")
	print("This system is remotely monitored and secured")
	print("https://porus.ca")
	print(datetime.now())
	time.sleep(1)
	print("==============================")
	print("1. Send Test Message")
	print("2. Verify / Change Contact Numbers")
	print("3. View recent logs")
	print("4. Check System Status")
	print("5. Check Sensor")
	print("6. End Session")
	print("==============================")
	selection=input("Enter choice: ")
	print("\n")
	if selection=="1":
		userInput = input("Enter your message: ")
		sendTestAlert(userInput)
		mainMenu()
	elif selection=="2":
		print("Coming Soon")
		time.sleep(1)
		mainMenu()
	elif selection=="3":
		print("Coming Soon")
		time.sleep(1)
		mainMenu()
	elif selection=="4":
		print("Coming Soon")
		time.sleep(1)
		mainMenu()
	elif selection=="5":
		print("Coming Soon")
		sensorTestInput = input("Please select the sensor # (1-5)")
		sesnsorTestConfirm = input("You have selected to test Sensor "+sensorTestInput+". Confirm? (Y/N)")
		if sesnsorTestConfirm=="y" or "Y":
			print("Begining test....")
			sensorStatusCheck("sensor"+sensorTestInput)
		time.sleep(1)
		mainMenu()
	elif selection=="6":
		print("Goodbye!")
		exit
	else:
		print("Please enter a valid selection.")
		time.sleep(1)
		mainMenu()

def login():
    user = input("Username: ")
    passw = getpass()
    f = open("config/users.cfg", "r")
    for line in f.readlines():
        us, pw = line.strip().split("|")
        if (user in us) and (passw in pw):
            print ("Login successful!")
            return True
    print ("Wrong username/password")
    time.sleep(5)
    return False

def main():
    log = login()
    if log == True:
         mainMenu()


os.system('clear')
print("Welcome to the SADASS Monitoring System")
print("Designed by Porus Consulting Group")
print("This system is remotely monitored and secured")
print("https://porus.ca")
print(datetime.now())
main()
